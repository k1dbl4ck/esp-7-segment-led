#include <Arduino.h>

#define LED D1

const int A = D1;
const int B = D2;
const int C = D3;
const int D = D4;
const int E = D5;
const int F = D6;
const int G = D7;
const int DP = D8;

void setup()
{
    pinMode(A, OUTPUT);
    pinMode(B, OUTPUT);
    pinMode(C, OUTPUT);
    pinMode(D, OUTPUT);
    pinMode(E, OUTPUT);
    pinMode(F, OUTPUT);
    pinMode(G, OUTPUT);
    pinMode(DP, OUTPUT);
}

extern int pins[8] = {D1, D2, D3, D4, D5, D6, D7, D8};
extern int numbers[10][8] = {
    {0, 0, 0, 0, 0, 0, 1, 1},
    {1, 0, 0, 1, 1, 1, 1, 1},
    {0, 0, 1, 0, 0, 1, 0, 1},
    {0, 0, 0, 0, 1, 1, 0, 1},
    {1, 0, 0, 1, 1, 0, 0, 1},
    {0, 1, 0, 0, 1, 0, 0, 1},
    {0, 1, 0, 0, 0, 0, 0, 1},
    {0, 0, 0, 1, 1, 1, 1, 1},
    {0, 0, 0, 0, 0, 0, 0, 1},
    {0, 0, 0, 0, 1, 0, 0, 1},
};

extern int loader[6][8] = {
    {0, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 1, 1, 1, 1, 1},
    {1, 1, 0, 1, 1, 1, 1, 1},
    {1, 1, 1, 0, 1, 1, 1, 1},
    {1, 1, 1, 1, 0, 1, 1, 1},
    {1, 1, 1, 1, 1, 0, 1, 1}};

extern int off[8] = {1, 1, 1, 1, 1, 1, 1, 1};

extern int cnt = 0;
void countdown()
{
    if (cnt < 0)
    {
        cnt = 9;
    }

    for (int i = 0; i < 8; i++)
    {
        digitalWrite(pins[i], numbers[cnt][i]);
    }
    cnt--;
}

void loading()
{

    if (cnt > 5)
    {
        cnt = 0;
    }

    for (int i = 0; i < 8; i++)
    {
        digitalWrite(pins[i], loader[cnt][i]);
    }
    cnt++;
}

void loop()
{
    //countdown();
    loading();
    delay(100);
}
